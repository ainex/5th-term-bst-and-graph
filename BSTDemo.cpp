#include <iostream>
#include <vector>
#include <functional>
#include <map>
#include "BinarySearchTree.h"

const std::string CREATE = "create";
const std::string PRINT = "print";
const std::string INSERT = "insert";
const std::string REMOVE = "remove";
const std::string FIND = "find";
const std::string SIZE = "size";
const std::string EMPTY = "empty";
const std::string CLEAR = "clear";
const std::string TRAVERSE = "trv";
const std::string INTERNAL_PATH = "path";
const std::string EXIT = "exit";
const std::string HELP = "help";

std::map<std::string, int> mapping;

void initCommands();


BinarySearchTree<int, int> *createTree();

template<class K, class T>
void exit(BinarySearchTree<K, T> *bst);

template<class K, class T>
void insert(BinarySearchTree<K, T> *bst);

template<class K, class T>
void remove(BinarySearchTree<K, T> *bst);

template<class K, class T>
void find(BinarySearchTree<K, T> *bst);

void help();


template<class K, class T>
BinarySearchTree<K, T> *handleCommand(BinarySearchTree<K, T> *binarySearchTree, int commandNumber);

int main() {
    initCommands();

    std::cout << "BinarySearchTree (BST) Demo" << std::endl;
    BinarySearchTree<int, int> *binarySearchTree = createTree();
    help();
    std::string command = "";

    while (true) {
        std::cout << ">> " << std::endl;
        std::cin >> command;
        int commandNumber = mapping[command];
        try {
            switch (commandNumber) {
                case 1: {
                    delete binarySearchTree;
                    binarySearchTree = createTree();
                    break;
                }
                case 2: {
                    binarySearchTree->printTree();
                    break;
                }
                case 3: {
                    insert(binarySearchTree);
                    break;
                }
                case 4: {
                    remove(binarySearchTree);
                    break;
                }
                case 5: {
                    find(binarySearchTree);
                    break;
                }
                case 6: {
                    std::cout << binarySearchTree->getSize() << std::endl;
                    break;
                }
                case 7: {
                    std::cout << binarySearchTree->isEmpty() << std::endl;
                    break;
                }
                case 8: {
                    binarySearchTree->clear();
                    break;
                }
                case 9: {
                    binarySearchTree->traverse();
                    break;
                }
                case 10: {
                    std::cout << binarySearchTree->internalPath() << std::endl;
                    break;
                }
                case 11: {
                    exit(binarySearchTree);
                    break;
                }
                case 12: {
                    help();
                    break;
                }
                default:
                    help();
            }
        }
        catch (const std::exception &e) {
            std::cerr << e.what() << std::endl;
        }
    }

}


BinarySearchTree<int, int> *createTree() {
    std::cout << "New BinarySearchTree (BST)" << std::endl;
    BinarySearchTree<int, int> *binarySearchTree = new BinarySearchTree<int, int>();

    binarySearchTree->insert(12, 12);
    binarySearchTree->insert(10, 10);
    binarySearchTree->insert(6, 6);
    binarySearchTree->insert(11, 11);
    binarySearchTree->insert(13, 13);
    binarySearchTree->insert(20, 20);
    binarySearchTree->insert(19, 19);
    binarySearchTree->insert(30, 30);
    binarySearchTree->insert(5, 5);
    binarySearchTree->insert(7, 7);
    binarySearchTree->printTree();
    return binarySearchTree;
}

template<class K, class T>
void exit(BinarySearchTree<K, T> *bst) {
    delete bst;
    exit(0);
}

void help() {
    std::cout << "Commands' List: " << std::endl;
    std::cout << CREATE << "\t  - create a new tree" << std::endl;
    std::cout << PRINT << "\t  - print tree" << std::endl;
    std::cout << INSERT << "\t  - insert a new node (k, v)" << std::endl;
    std::cout << REMOVE << "\t  - remove a node by a key" << std::endl;
    std::cout << FIND << "\t  - find a node data by a key" << std::endl;
    std::cout << SIZE << "\t  - get a tree size (node count)" << std::endl;
    std::cout << EMPTY << "\t  - check if a tree is empty" << std::endl;
    std::cout << CLEAR << "\t  - remove all nodes from a tree" << std::endl;
    std::cout << TRAVERSE << "\t  - traverse a tree" << std::endl;
    std::cout << INTERNAL_PATH << "\t  - get an internal path length of a three" << std::endl;
    std::cout << EXIT << "\t  - exit" << std::endl;
    std::cout << HELP << "\t  - repeat this message" << std::endl;

}

void initCommands() {
    mapping[CREATE] = 1;
    mapping[PRINT] = 2;
    mapping[INSERT] = 3;
    mapping[REMOVE] = 4;
    mapping[FIND] = 5;
    mapping[SIZE] = 6;
    mapping[EMPTY] = 7;
    mapping[CLEAR] = 8;
    mapping[TRAVERSE] = 9;
    mapping[INTERNAL_PATH] = 10;
    mapping[EXIT] = 11;
    mapping[HELP] = 12;
}

template<class K, class T>
void insert(BinarySearchTree<K, T> *bst) {
    std::cout << "insert a new  key and value: k v " << std::endl;
    K key;
    T value;
    std::cin >> key >> value;
    bst->insert(key, value);

}

template<class K, class T>
void remove(BinarySearchTree<K, T> *bst) {
    std::cout << "insert a key to remove: " << std::endl;
    K key;
    std::cin >> key;
    bst->remove(key);
}

template<class K, class T>
void find(BinarySearchTree<K, T> *bst) {
    std::cout << "insert a key to find: " << std::endl;
    K key;
    T data;
    std::cin >> key;
    data = bst->find(key);
    std::cout << "found data: " << data << std::endl;
}
