cmake_minimum_required(VERSION 3.9)
project(bst_and_graph)

set(CMAKE_CXX_STANDARD 11)

add_executable(bst_demo BSTDemo.cpp BinarySearchTree.h BinarySearchTreeException.h)
add_executable(graph_demo Digraph.h DigraphException.h DigraphDemo.cpp GraphDistanceTask.h)
add_executable(hash_table_demo HashTable.h HashTableDemo.cpp Digraph.h DigraphException.h GraphDistanceTask.h HashTableException.h HashTableTest.h)