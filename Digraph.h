#ifndef BST_AND_GRAPH_DIGRAPH_H
#define BST_AND_GRAPH_DIGRAPH_H


#include <sstream>
#include <list>
#include <queue>
#include "DigraphException.h"


template<class T, class W>
class GraphDistanceTask;

template<class T, class W>
class Digraph {

    friend class GraphDistanceTask<T, W>;

public:
    /**
     * Максимальное количество вершин
     * @return максимальное количество вершин
     */
    static int getMaxVertexAmount();

    /**
     * Максимальное количество ребер
     * @return максимум для количества ребер
     */
    static int getMaxEdgeAmount();

    /**
     * конструктор
     * @param vertexAmount количество вершин
     */
    explicit Digraph(int vertexAmount);

    //деструктор
    ~Digraph();

    /**
     * опрос числа вершин в графе
     * @return количество вершин
     */
    int getVertexAmount();

    /**
     * опрос числа ребер в графе
     * @return текущее количество ребер
     */
    int getEdgeAmount();

    /**
     * вставка ребра, соединяющего вершины v1, v2
     * @param v1 начальная вершина
     * @param v2 конечная вершина
     * @param data данные
     * @param weight вес ребра
     * @throw multipleEdgeException если ребро уже существует
     */
    void insertEdge(int v1, int v2, T data, W weight);

    /**
     * удаление ребра, соединяющего вершины v1, v2
     * @param v1 начальная вершина
     * @param v2 конечная вершина
     * @throw noSuchEdgeException если ребро не найдено
     */
    void deleteEdge(int v1, int v2);

    /**
     * опрос наличия ребра, соединяющего вершины v1, v2
     * @param v1 начальная вершина
     * @param v2 конечная вершина
     * @return истина - ребро существует, ложь - ребро не существует
     */
    bool isEdge(int v1, int v2);

    /**
     * задание параметров ребра
     * @param v1 начальная вершина
     * @param v2 конечная вершина
     * @param data
     * @throw noSuchEdgeException если ребро не найдено
     */
    void setEdge(int v1, int v2, T data);

    /**
     * Последовательный вывод элементов списка смежности для каждой вершины. 
     * Вид [v1] -> [v2|data|weight] ->...->[vn|data|weight]
     */
    void print();

    /**
     * Выполнение задачи на данном графе
     * @param task задача
     */
    void doTask(GraphDistanceTask<T, W> *task);


    //внутренний класс для Ребра
    class Edge {

    public:
        int vertex; //вершина
        T data; //данные
        W weight; //вес ребра
        Edge *next;    //указатель на следующее ребро

        Edge(int vertex, T data, W weight);
    };

    //срытые поля  и структуры Graph
protected:
    static const int MAX_EDGE_AMOUNT = 30;
    static const int MAX_VERTEX_AMOUNT = 20;

    int vertexAmount;
    int edgeAmount;
    Edge *adjacencyList[MAX_VERTEX_AMOUNT] = {nullptr};
    GraphDistanceTask<T, W> *task = nullptr;

    /**
     * валидация вершины.
     * проверка, что не превышает заданное количество вершин
     * @param vertex вершина
     */
    void validateVertex(int vertex);

};

template<class T, class W>
Digraph<T, W>::Digraph(int vertexAmount) {
    if (vertexAmount < 1 || vertexAmount > MAX_VERTEX_AMOUNT) {
        throw invalidVertexAmountException;
    }
    this->vertexAmount = vertexAmount;
    this->edgeAmount = 0;
    this->task = nullptr;
}

template<class T, class W>
Digraph<T, W>::~Digraph() {
   for(int i=0; i< MAX_VERTEX_AMOUNT; i++){
       if(adjacencyList[i] == nullptr){
           continue;
       } else {
           Edge *edge = adjacencyList[i];
           while (edge != nullptr){
               Edge *next = edge->next;
               delete edge;
               edge = next;
           }
       }
   }
}

template<class T, class W>
Digraph<T, W>::Edge::Edge(int vertex, T data, W weight) {
    this->vertex = vertex;
    this->data = data;
    this->weight = weight;
    this->next = nullptr;
}

template<class T, class W>
int Digraph<T, W>::getVertexAmount() {
    return this->vertexAmount;
}

template<class T, class W>
int Digraph<T, W>::getMaxVertexAmount() {
    return MAX_VERTEX_AMOUNT;
}

template<class T, class W>
int Digraph<T, W>::getMaxEdgeAmount() {
    return MAX_EDGE_AMOUNT;
}

template<class T, class W>
int Digraph<T, W>::getEdgeAmount() {
    return this->edgeAmount;
}

template<class T, class W>
void Digraph<T, W>::insertEdge(int v1, int v2, T data, W weight) {
    //предусловия
    validateVertex(v1);
    validateVertex(v2);
    if (v1 == v2) {
        throw loopEdgeException;
    }
    if (isEdge(v1, v2)) {
        throw multipleEdgeException;
    }
    Edge *edge = new Edge(v2, data, weight);

    Edge *existingEdge = adjacencyList[v1 - 1];
    if (nullptr == existingEdge) {
        adjacencyList[v1 - 1] = edge;
    } else {
        while (nullptr != existingEdge->next) {
            existingEdge = existingEdge->next;
        }
        existingEdge->next = edge;
    }
    edgeAmount++;
}

template<class T, class W>
bool Digraph<T, W>::isEdge(int v1, int v2) {
    validateVertex(v1);
    validateVertex(v2);

    Edge *edge = adjacencyList[v1 - 1];
    if (nullptr == edge) { //нет ребер
        return false;
    } else {
        while (nullptr != edge) {
            if (edge->vertex == v2) {
                return true;
            }
            edge = edge->next;
        }
        return false;
    }

}

template<class T, class W>
void Digraph<T, W>::validateVertex(int vertex) {
    if (vertex > getVertexAmount() || vertex < 1) {
        throw invalidVertexException;
    }
}

template<class T, class W>
void Digraph<T, W>::deleteEdge(int v1, int v2) {

    validateVertex(v1);
    validateVertex(v2);

    int vertexIndex = v1 - 1;
    Edge *edgeDeleted = adjacencyList[vertexIndex];
    Edge *previous = edgeDeleted;
    while (nullptr != edgeDeleted && edgeDeleted->vertex != v2) {
        previous = edgeDeleted;
        edgeDeleted = edgeDeleted->next;
    }
    if (nullptr == edgeDeleted) {
        throw noSuchEdgeException;
    }

    //сохраняем указатель на следующее за удаляемым ребро, чтобы установить это значение в предыдущее ребро
    //может быть null
    Edge *nextAfterDeleted = edgeDeleted->next;
    if (previous == edgeDeleted) { //единственное  ребро в списке
        adjacencyList[vertexIndex] = nextAfterDeleted;
    } else {
        previous->next = nextAfterDeleted;
    }
    delete edgeDeleted;

    edgeAmount--;
}

template<class T, class W>
void Digraph<T, W>::setEdge(int v1, int v2, T data) {
    if (!isEdge(v1, v2)) {
        throw noSuchEdgeException;
    }
    int vertexIndex = v1 - 1;
    Edge *edgeUpdated = adjacencyList[vertexIndex];

    while (edgeUpdated->vertex != v2) {
        edgeUpdated = edgeUpdated->next;
    }
    edgeUpdated->data = data;
}

template<class T, class W>
void Digraph<T, W>::print() {

    std::cout << "Graph has " << vertexAmount << " vertices" << std::endl;
    if (0 == edgeAmount) {
        std::cout << "Graph has no edges" << std::endl;
        return;
    }
    std::cout << "Graph has " << edgeAmount << " edges" << std::endl;

    std::cout << "An adjacency list representation for a Graph" << std::endl;
    std::cout << "[v1] -> [v2|data|weight] ->...->[vn|data|weight]" << std::endl;
    std::cout << "Each element of the list describes the set of neighbors of a vertex in the graph:" << std::endl
              << std::endl;

    std::stringstream ss;
    for (int i = 0; i < vertexAmount; i++) {
        Edge *edge = adjacencyList[i];
        ss << "[" << i + 1 << "]";
        while (nullptr != edge) {
            ss << "->" << "[" << edge->vertex << "|" << edge->data << "|" << edge->weight << "]";
            edge = edge->next;
        }
        ss << std::endl;
    }
    std::string edgesInfo(ss.str());
    std::cout << edgesInfo;

}

template<class T, class W>
void Digraph<T, W>::doTask(GraphDistanceTask<T, W> *task) {
    this->task = task;
    std::vector<int> vector = this->task->execute();
    for (std::vector<int>::iterator it = vector.begin(); it != vector.end(); ++it) {
        std::cout << *it << " ";
    }
    std::cout << std::endl;
}

#endif //BST_AND_GRAPH_DIGRAPH_H
