#ifndef BST_AND_GRAPH_GRAPHDISTANCETASK_H
#define BST_AND_GRAPH_GRAPHDISTANCETASK_H


#include <vector>
#include <limits>
#include "Digraph.h"
//
//class Digraph;


template<class T, class W>
class GraphDistanceTask {

protected:
    Digraph<T, W> *digraph;
    int startVertex;
    W maxDistance;

    //внутренний класс для Вершины
    class Vertex {
    public:
        int vertexNumber;
        W distance;
        int previousVertex;
        bool checked;

        Vertex(int vertexNumber, W distance);
    };

    void initializeVertices(int startVertex, Vertex **vertices, int amount);

    int findByMinDistance(Vertex **vertices, int amount);

public:
    explicit GraphDistanceTask(int startVertex, W distance);

    ~GraphDistanceTask();

    void setDigraph(Digraph<T, W> *digraph);

    /**
    * Алгоритм определения вершин, удаленных от заданной вершины в пределах заданного по весу расстояния.
    * @param startVertex - начальная вершина
    * @param distanceLimit - предел по весу
    * @return номера вершин, удаленных от указанной вершины в пределах заданного расстояния
    */
    std::vector<int> execute();


    /**
     * Релаксация ребра (u, v)E состоит в следующем: d[v] уменьшается до d[u] + w(u,v), если эта сумма меньше d[v].
     * Новое значение становится верхней оценкой кратчайшего пути, а вершина u становится родителем вершины v, то есть, p[v] = u.
     * @param vertex вершина, для которой будет проведена релаксация всех соответствующих  ребер
     * @param vertices массив всех исследуемых вершин
     */
    void doEdgesRelaxation(Vertex *vertex, GraphDistanceTask<T, W>::Vertex **vertices);
};

template<class T, class W>
GraphDistanceTask<T, W>::Vertex::Vertex(int vertexNumber, W distance) {
    this->vertexNumber = vertexNumber;
    this->distance = distance;
    this->previousVertex = 0;
    this->checked = false;
}

template<class T, class W>
GraphDistanceTask<T, W>::GraphDistanceTask(int startVertex, W distance) {
    this->startVertex = startVertex;
    this->maxDistance = distance;
    this->digraph = nullptr;
}

template<class T, class W>
GraphDistanceTask<T, W>::~GraphDistanceTask() {

}

template<class T, class W>
std::vector<int>
GraphDistanceTask<T, W>::execute() {
    int vertexAmount = digraph->getVertexAmount();
    Vertex *vertices[vertexAmount] = {nullptr};
    initializeVertices(startVertex, vertices, vertexAmount);
    std::vector<int> checkedVertices;

    //пока остались вершины, расстояние до которых еще не рассчитано
    int vertexNumber = findByMinDistance(vertices, vertexAmount);
    while (vertexNumber != 0) {

        Vertex *vertex = vertices[vertexNumber - 1];
        //для каждой вершины в списке смежности делаем релаксацию ребра
        doEdgesRelaxation(vertex, vertices);
        //отмечаем вершину, как исследованную
        checkedVertices.push_back(vertexNumber);
        vertex->checked = true;
        //находим следующую вершину с минимальным расстоянием
        vertexNumber = findByMinDistance(vertices, vertexAmount);
    }

    std::vector<int> reachableVertices;
    for (int i = 0; i < vertexAmount; i++) {
        Vertex *vertex = vertices[i];
        if (vertex->vertexNumber == startVertex) {
            continue;
        }
        if (vertex->distance <= this->maxDistance) {
            reachableVertices.push_back(vertex->vertexNumber);
        }
    }


    return reachableVertices;
}

template<class T, class W>
void GraphDistanceTask<T, W>::initializeVertices(int startVertex, GraphDistanceTask::Vertex **vertices, int amount) {
    int intMax = std::numeric_limits<int>::max();
    for (int i = 0; i < amount; i++) {
        int vertexNumber = i + 1;
        vertices[i] = new Vertex(vertexNumber, intMax);

    }
    vertices[startVertex - 1]->distance = 0;
}

template<class T, class W>
void GraphDistanceTask<T, W>::setDigraph(Digraph<T, W> *digraph) {
    this->digraph = digraph;
}

template<class T, class W>
int GraphDistanceTask<T, W>::findByMinDistance(GraphDistanceTask::Vertex **vertices, int amount) {
    //ищем первую не проверенную вершину и принимаем ее значение расстояния за минимальное
    int minVertexIndex = -1;
    long minDistance = std::numeric_limits<long>::max();

    for (int i = 0; i < amount; i++) {
        if (!vertices[i]->checked && vertices[i]->distance < minDistance) {
            minDistance = vertices[i]->distance;
            minVertexIndex = i;
        }
    }
    return minVertexIndex + 1;

}

template<class T, class W>
void GraphDistanceTask<T, W>::doEdgesRelaxation(GraphDistanceTask<T, W>::Vertex *vertex,
                                                GraphDistanceTask<T, W>::Vertex **vertices) {

    typename Digraph<T, W>::Edge *edge = digraph->adjacencyList[vertex->vertexNumber - 1];

    while (edge != nullptr) {
        int adjustedVertexNumber = edge->vertex;
        Vertex *adjustedVertex = vertices[adjustedVertexNumber - 1];
        if (adjustedVertex->distance > vertex->distance + edge->weight) {
            adjustedVertex->distance = vertex->distance + edge->weight;
            adjustedVertex->previousVertex = vertex->vertexNumber;
        }
        edge = edge->next;
    }

}


#endif //BST_AND_GRAPH_GRAPHDISTANCETASK_H
