#ifndef BST_AND_GRAPH_HASHTABLEEXCEPTION_H
#define BST_AND_GRAPH_HASHTABLEEXCEPTION_H

#include <exception>

class NoSuchKey: public std::exception
{
    virtual const char* what() const throw()
    {
        return "Error: There is no such key in a hash table";
    }

} noSuchKeyException;

class NoSuchElement: public std::exception
{
    virtual const char* what() const throw()
    {
        return "Error: There is no such element";
    }

} noSuchElementException;

#endif //BST_AND_GRAPH_HASHTABLEEXCEPTION_H
