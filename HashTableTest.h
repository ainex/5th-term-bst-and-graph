#ifndef BST_AND_GRAPH_HASHTABLETEST_H
#define BST_AND_GRAPH_HASHTABLETEST_H


#include <random>
#include <iostream>
#include "HashTable.h"

template<class K, class T>
class HashTableTest {

public:
    HashTableTest();

    double getRandomKey();

    void testOperations(double loadFactor, HashTable<K, T> *table);

protected:
    std::random_device rd;
    double rangeStart;
    double rangeEnd;
    std::mt19937 engine;

};

template<class K, class T>
HashTableTest<K, T>::HashTableTest() {
    rangeStart = 100000.00d;
    rangeEnd = 150000.00d;
    engine.seed(rd());
}

template<class K, class T>
void HashTableTest<K, T>::testOperations(double loadFactor, HashTable<K, T> *table) {
    int needToGenerate = (int) table->getTableSize() * loadFactor;
    std::cout << "need to generate: " << needToGenerate << std::endl;

    for (int n = 0; n < needToGenerate; ++n) {
        double randomKey = getRandomKey();
        table->insert(randomKey, 1);
    }
    std::cout << "test table is ready with load factor: " << table->getLoadFactor() << std::endl;

    int insertProbeCounter = 0, findProbeCounter = 0, deleteProbeCounter = 0;
    double previousKey = getRandomKey();
    for (int n = 0; n < 1000; ++n) {
        double randomKey = getRandomKey();
        if (n % 10 != 0) {
            table->insert(randomKey, 100);
            insertProbeCounter += table->getProbeCount();

            try {
                table->find(randomKey);
            }
            catch (const std::exception &e) {
                //silent
            }

            findProbeCounter += table->getProbeCount();

            table->remove(randomKey);
            deleteProbeCounter += table->getProbeCount();
        } else {
            table->insert(previousKey, 666);
            insertProbeCounter += table->getProbeCount();

            try {
                table->find(randomKey);
            }
            catch (const std::exception &e) {
                //silent
            }
            findProbeCounter += table->getProbeCount();

            table->remove(randomKey);
            deleteProbeCounter += table->getProbeCount();
            table->remove(previousKey);

        }
        previousKey = randomKey;
    }
    std::cout << "test table after operations load factor: " << table->getLoadFactor() << std::endl;
    std::cout << "results insert/find/delete: " << insertProbeCounter << " " << findProbeCounter << " "
              << deleteProbeCounter << std::endl;
    std::cout << " avr: " << (insertProbeCounter + findProbeCounter + deleteProbeCounter) / 3000.0f << std::endl;
}

template<class K, class T>
double HashTableTest<K, T>::getRandomKey() {
    // Distributions
    std::uniform_real_distribution<double> dist(rangeStart, rangeEnd);
    double randomKey = dist(engine);
    return randomKey;
}


#endif //BST_AND_GRAPH_HASHTABLETEST_H
