#ifndef BST_AND_GRAPH_BINARYSEARCHTREEEXCEPTION_H
#define BST_AND_GRAPH_BINARYSEARCHTREEEXCEPTION_H
#include <exception>

class InvalidVertexAmountException: public std::exception
{
    virtual const char* what() const throw()
    {
        return "Error: Invalid vertex amount ";
    }

} invalidVertexAmountException;

class InvalidVertexException: public std::exception
{
    virtual const char* what() const throw()
    {
        return "Error: Invalid vertex ";
    }

} invalidVertexException;

class LoopEdgeException: public std::exception
{
    virtual const char* what() const throw()
    {
        return "Error: The edge connects the vertex to itself (a loop), which is not allowed in a simple graph";
    }

} loopEdgeException;

class MultipleEdgeException: public std::exception
{
    virtual const char* what() const throw()
    {
        return "Error: The edge is already exist. Multiple edges between the same vertices is not allowed in a simple graph";
    }

} multipleEdgeException;

class NoSuchEdgeException: public std::exception
{
    virtual const char* what() const throw()
    {
        return "Error: There is no such edge in a graph";
    }

} noSuchEdgeException;



#endif //BST_AND_GRAPH_BINARYSEARCHTREEEXCEPTION_H
