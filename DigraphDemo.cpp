#include <iostream>
#include <vector>
#include <functional>
#include <map>
#include "Digraph.h"
#include "GraphDistanceTask.h"

const std::string CREATE = "create";
const std::string PRINT = "print";
const std::string INSERT = "insert";
const std::string DELETE = "delete";
const std::string SET = "set";
const std::string EDGE = "edge";
const std::string FIND = "find";
const std::string EXIT = "exit";
const std::string HELP = "help";

std::map<std::string, int> mapping;

void initCommands();

Digraph<int, int> *createGraph();

void help();

template<class T, class W>
void insertEdge(Digraph<T, W> *digraph);

template<class T, class W>
void deleteEdge(Digraph<T, W> *digraph);

template<class T, class W>
void setEdge(Digraph<T, W> *digraph);

template<class T, class W>
void isEdge(Digraph<T, W> *digraph);

template<class T, class W>
void findVertices(Digraph<T, W> *digraph);

template<class T, class W>
void exit(Digraph<T, W> *digraph);

int main() {
    initCommands();

    std::cout << "Weighted Directed (Digraph) Demo" << std::endl;
    Digraph<int, int> *digraph = new Digraph<int, int>(6);
    digraph->insertEdge(1, 2, 55, 10);
    digraph->insertEdge(1, 3, 66, 30);
    digraph->insertEdge(2, 3, 77, 10);
    digraph->insertEdge(3, 2, 88, 5);
    digraph->insertEdge(3, 4, 88, 10);
    digraph->insertEdge(4, 5, 11, 20);
    digraph->insertEdge(4, 6, 12, 20);

    help();

    std::string command = "";
    while (true) {

        std::cout << ">> " << std::endl;
        std::cin >> command;
        int commandNumber = mapping[command];
        try {
            switch (commandNumber) {
                case 1: {
                    delete digraph;
                    digraph = createGraph();
                    break;
                }
                case 2: {
                    digraph->print();
                    break;
                }
                case 3: {
                    insertEdge(digraph);
                    break;
                }
                case 4: {
                    deleteEdge(digraph);
                    break;
                }
                case 5: {
                    setEdge(digraph);
                    break;
                }
                case 6: {
                    isEdge(digraph);
                    break;
                }
                case 7: {
                    findVertices(digraph);
                    break;
                }
                case 8: {
                    exit(digraph);
                    break;
                }
                default:
                    help();
            }
        }
        catch (const std::exception &e) {
            std::cerr << e.what() << std::endl;
        }
    }

}

void initCommands() {
    mapping[CREATE] = 1;
    mapping[PRINT] = 2;
    mapping[INSERT] = 3;
    mapping[DELETE] = 4;
    mapping[SET] = 5;
    mapping[EDGE] = 6;
    mapping[FIND] = 7;
    mapping[EXIT] = 8;
    mapping[HELP] = 9;
}

template<class T, class W>
void exit(Digraph<T, W> *digraph) {
    delete digraph;
    exit(0);
}

void help() {
    std::cout << "Commands' List: " << std::endl;
    std::cout << CREATE << "\t  - create a new graph" << std::endl;
    std::cout << PRINT << "\t  - print Adjacency lists" << std::endl;
    std::cout << INSERT << "\t  - insert a new  edge (v1, v2, data, weight)" << std::endl;
    std::cout << DELETE << "\t  - delete an edge" << std::endl;
    std::cout << SET << "\t  - set edge data" << std::endl;
    std::cout << EDGE << "\t  - check if there is an edge (v1, v2)" << std::endl;
    std::cout << FIND << "\t  - find all vertexes limited by path weight" << std::endl;

    std::cout << EXIT << "\t  - exit" << std::endl;
    std::cout << HELP << "\t  - repeat this message" << std::endl;

}

template<class T, class W>
void insertEdge(Digraph<T, W> *digraph) {
    std::cout << "insert a new edge params (v1, v2, data, weight)" << std::endl;
    T data;
    W weight;
    int v1, v2;
    std::cin >> v1 >> v2 >> data >> weight;
    digraph->insertEdge(v1, v2, data, weight);
    std::cout << "new edge " << v1 << "-" << v2 << " was added" << std::endl;
}

template<class T, class W>
void deleteEdge(Digraph<T, W> *digraph) {
    std::cout << "insert an  edge to delete (v1, v2)" << std::endl;
    int v1, v2;
    std::cin >> v1 >> v2;
    digraph->deleteEdge(v1, v2);
    std::cout << "the edge " << v1 << "-" << v2 << " was deleted" << std::endl;
}

template<class T, class W>
void isEdge(Digraph<T, W> *digraph) {
    std::cout << "insert an edge to check (v1, v2)" << std::endl;
    int v1, v2;
    std::cin >> v1 >> v2;
    bool isEdge = digraph->isEdge(v1, v2);
    std::cout << "the edge " << (isEdge ? "exists" : "does not exist") << std::endl;
}


Digraph<int, int> *createGraph() {
    int vertexAmount;
    std::cout << "Input vertex amount, max " << Digraph<int, int>::getMaxVertexAmount() << std::endl;
    std::cin >> vertexAmount;

    Digraph<int, int> *digraph = new Digraph<int, int>(vertexAmount);
    std::cout << "A new graph was created, vertices: " << digraph->getVertexAmount() << " edges: "
              << digraph->getEdgeAmount() << std::endl;
    return digraph;
}

template<class T, class W>
void setEdge(Digraph<T, W> *digraph) {
    std::cout << "enter a new edge data (v1, v2, data)" << std::endl;
    T data;
    int v1, v2;
    std::cin >> v1 >> v2 >> data;
    digraph->setEdge(v1, v2, data);
    std::cout << "data of the edge " << v1 << "-" << v2 << " was updated" << std::endl;
}

template<class T, class W>
void findVertices(Digraph<T, W> *digraph) {
    std::cout << "enter a start vertex and limit (v, weight)" << std::endl;
    W distanceLimit;
    int vertex;
    std::cin >> vertex >> distanceLimit;
    GraphDistanceTask<T,W> *graphDistanceTask = new GraphDistanceTask<T,W>(vertex, distanceLimit);
    graphDistanceTask->setDigraph(digraph);

    std::cout << "reachable vertices: " << std::endl;
    digraph->doTask(graphDistanceTask);

}


