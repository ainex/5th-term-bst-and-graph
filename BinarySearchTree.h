#ifndef BST_AND_GRAPH_BINARYSEARCHTREE_H
#define BST_AND_GRAPH_BINARYSEARCHTREE_H

#include <iostream>
#include <stack>
#include "BinarySearchTreeException.h"


template<class K, class T>
class BinarySearchTree {
//скрытая секция объекта
//срытые поля  и структуры Дерева
protected:
    //внутренний класс для элемента коллекции
    class Node {
    public:
        K key;      //ключ узла
        T data;    //данные узла
        Node *left;    //указатель на левого ребенка
        Node *right; //указатель на правого ребенка

        Node(K key, T data);
    };

    //аттрибуты класса

    //количество узлов в дереве
    int size;
    //указатель на корень дерева
    Node *root;

    //вспомогательные методы

    //количество детей узла
    int childrenAmount(Node *pNode);

    //обновить ссылку на ребенка в родителе
    void updateChildReferenceInParent(Node *parent, Node *child, Node *newChild);

    //удалить и вставить замену
    void removeAndInsertReplacement(Node *removingNode, BinarySearchTree::Node *removingParentNode);

    //подсчет длины внутреннего пути, рекурсивно
    int internalPathCount(Node *node, int level);

    //печать дерева
    void printTree(BinarySearchTree::Node *node, int level);

//открытая секция объекта коллекции
public:
    BinarySearchTree();    //конструктор по умолчанию
    ~BinarySearchTree();    //деструктор

    //объявление прототипов интерфейсных методов


    /**
     * опрос размера дерева (количества узлов)
     * @return количество узлов дерева
     */
    int getSize();

    /**
     * очистка дерева (удаление всех узлов)
     */
    void clear();

    /**
     * проверка дерева на пустоту
     * @return истина - дерево пустое, ложь - дерево не пустое
     */
    bool isEmpty();

    /**
     * поиск данных с заданным ключом
     * @param key значение ключа
     * @return значение элемента в дереве с заданным ключом
     * @throws исключение noSuchKeyException, если узел с заданным ключом не найден
     */
    T find(K key);

    /**
     * включение в дерево нового узла с заданным ключом и данными
     * @param key ключ
     * @param data данные
     * @return признак упешности вставки, истина - узел добавлен, ложь - узел не добавлен (уже существует)
     */
    bool insert(K key, T data);

    /**
     * удаление из дерева узла с заданным ключом
     * @param key ключ
     * @return признак успешности удаления, истина - узел удален
     * ложь - узел не удален (не найден)
     */
    bool remove(K key);

    /**
     * Обход узлов в дереве по схеме: t → Lt → Rt.
     * Префиксный обход  (сверху вниз), при котором обрабатывается значение в узле,
     * а затем значения из левого и правого поддеревьев узла.
     */
    void traverse();

    /**
     * Дополнительная операция
     * Определение длины внутреннего пути дерева (длина внутреннего пути = сумма глубин внутренних узлов)
     * @return сумму глубин внутренних узлов
     */
    int internalPath();

    /**
     * Вывод дерева на печать
     */
    void printTree();
};

template<class K, class T>
BinarySearchTree<K, T>::Node::Node(K key, T data) {
    this->key = key;
    this->data = data;
    this->left = NULL;
    this->right = NULL;
}

template<class K, class T>
BinarySearchTree<K, T>::BinarySearchTree() {
    this->root = NULL;
    this->size = 0;
}

template<class K, class T>
int BinarySearchTree<K, T>::getSize() {
    return size;
}

template<class K, class T>
bool BinarySearchTree<K, T>::isEmpty() {
    return 0 == size;
}

template<class K, class T>
T BinarySearchTree<K, T>::find(K key) {
    if (isEmpty()) {
        throw emptyTreeException;
    }
    Node *current = root;
    while (NULL != current && key != current->key) {
        if (key < current->key) {
            current = current->left;
        } else {
            current = current->right;
        }
    }
    if (NULL == current) {
        throw noSuchKeyException;
    }
    return current->data;

}

template<class K, class T>
bool BinarySearchTree<K, T>::insert(K key, T data) {

    if (NULL == root) {
        root = new Node(key, data);
        size++;
        return true;
    }

    Node *current = root;
    Node *parent = root;
    while (NULL != current) {
        parent = current;
        //элемент уже существует
        if (key == current->key) {
            return false;
        }
        //переходим в соответствующее поддерево
        if (key < current->key) {
            current = current->left;
        } else {
            current = current->right;
        }
    }
    //нашли место для вставки, current == NULL
    Node *newNode = new Node(key, data);

    //определяем в какое поддерево родителя вставить новый узел
    if (key < parent->key) {
        parent->left = newNode;
    } else {
        parent->right = newNode;
    }

    size++;
    return true;
}

template<class K, class T>
void BinarySearchTree<K, T>::traverse() {
    std::stack<Node *> nodePointerStack;    //вспомогательный стэк
    if (isEmpty()) {
        throw emptyTreeException;
    }
    std::cout << "Pre-order Traversal: t -> Lt -> Rt" << std::endl;
    Node *current;
    nodePointerStack.push(root);
    while (!nodePointerStack.empty()) {
        current = nodePointerStack.top();
        nodePointerStack.pop();

        if (NULL != current->right) {
            nodePointerStack.push(current->right);
        }
        if (NULL != current->left) {
            nodePointerStack.push(current->left);
        }

        //вывод ключа
        std::cout << current->key << " ";
    }
    std::cout << std::endl;
}

template<class K, class T>
bool BinarySearchTree<K, T>::remove(K key) {
    if (isEmpty()) {
        throw emptyTreeException;
    }
    Node *current = root;
    Node *parent = root;
    while (NULL != current && key != current->key) {
        parent = current;
        if (key < current->key) {
            current = current->left;
        } else {
            current = current->right;
        }
    }
    if (NULL == current) {
        return false;
    }
    //узел найден. при удалении возможны три варианта
    // 1) узел является листом. освобождаем память, стираем укзазатель из родителя
    if (0 == childrenAmount(current)) {
        updateChildReferenceInParent(parent, current, NULL);

        //2) у узла один ребенок. удаляем узел. родитель ссылается на ребенка
    } else if (1 == childrenAmount(current)) {
        Node *newChild = NULL;
        if (current->left != NULL) {
            newChild = current->left;
        } else {
            newChild = current->right;
        }
        updateChildReferenceInParent(parent, current, newChild);
        //узел содержит два ребенка
    } else if (2 == childrenAmount(current)) {
        removeAndInsertReplacement(current, parent);
    }

    //освобождаем память, уменьшаем колиество узлов
    delete current;
    size--;
    return true;
}

template<class K, class T>
void BinarySearchTree<K, T>::clear() {
    std::stack<Node *> nodePointerStack;    //вспомогательный стэк
    if (isEmpty()) {
        return;
    }
    Node *current;
    nodePointerStack.push(root);
    while (!nodePointerStack.empty()) {
        current = nodePointerStack.top();
        nodePointerStack.pop();

        if (NULL != current->right) {
            nodePointerStack.push(current->right);
        }
        if (NULL != current->left) {
            nodePointerStack.push(current->left);
        }

        //освобождение памяти
        delete current;
    }
    root = NULL;
    size = 0;
}

template<class K, class T>
int BinarySearchTree<K, T>::childrenAmount(BinarySearchTree::Node *pNode) {
    if (pNode->left == NULL && pNode->right == NULL) { return 0; }
    else if (pNode->left != NULL && pNode->right != NULL) { return 2; }
    else { return 1; }
}

template<class K, class T>
void
BinarySearchTree<K, T>::updateChildReferenceInParent(BinarySearchTree::Node *parent, BinarySearchTree::Node *child,
                                                     BinarySearchTree::Node *newChild) {

    if (root == child) {
        root = newChild;
    } else {

        if (parent->left == child) {
            parent->left = newChild;
        }
        if (parent->right == child) {
            parent->right = newChild;
        }
    }
}

template<class K, class T>
void BinarySearchTree<K, T>::removeAndInsertReplacement(BinarySearchTree::Node *removingNode,
                                                        BinarySearchTree::Node *removingParentNode) {
    //Замена - это узел с минимальным ключом в правом поддереве удаляемого узла.
    // У замещающего узла него нет левого сына.
    // Затем копируются ключ и данные из вершины замещающего узла в удаляемый узел и замещающий узел удаляется из дерева.

    //переходим на правое поддерево
    Node *replacement = removingNode->right;
    Node *replacementParent = removingNode;
    while (replacement->left != NULL) {
        replacementParent = replacement;
        replacement = replacement->left;
    }
    //открепляем замену от родителя. родитель теперь ссылается на правого потомка замены, если такой есть
    updateChildReferenceInParent(replacementParent, replacement, replacement->right);

    //копируем ссылки на детей в замену из удаляемого узла
    replacement->left = removingNode->left;
    replacement->right = removingNode->right;

    //родитель удаляемого узла ссылается на замену
    updateChildReferenceInParent(removingParentNode, removingNode, replacement);
}

template<class K, class T>
BinarySearchTree<K, T>::~BinarySearchTree() {
    if (!isEmpty()) {
        clear();
    }
}

template<class K, class T>
int BinarySearchTree<K, T>::internalPath() {
    return internalPathCount(root, 0);
}

template<class K, class T>
void BinarySearchTree<K, T>::printTree() {
    printTree(root, 0);
}

template<class K, class T>
int BinarySearchTree<K, T>::internalPathCount(BinarySearchTree::Node *node, int level) {
    if (node != root) {
        level++;
    };

    if (NULL == node) {
        return 0;
    } else if (2 == childrenAmount(node)) {
        return level + internalPathCount(node->left, level) + internalPathCount(node->right, level);
    } else {
        return internalPathCount(node->left, level) + internalPathCount(node->right, level);
    }
}

template<class K, class T>
void BinarySearchTree<K, T>::printTree(BinarySearchTree::Node *node, int level) {
    if (node != NULL) {
        printTree(node->left, level + 1);
        for (int i = 0; i < level; i++) std::cout << "   ";
        std::cout << node->key << std::endl;
        printTree(node->right, level + 1);
    }
}

#endif //BST_AND_GRAPH_BINARYSEARCHTREE_H
