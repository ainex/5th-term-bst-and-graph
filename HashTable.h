#ifndef BST_AND_GRAPH_HASHTABLE_H
#define BST_AND_GRAPH_HASHTABLE_H

#include <cmath>
#include <limits>
#include <string>
#include "HashTableException.h"

template<class K, class T>
class HashTable {
public:
    enum State {
        free, busy, deleted
    };

    /**
     * хэш функция для заданного ключа
     * @param key ключ
     * @return хэш значение ключа
     */
    int hash(K key);

    /**
     * опрос размера таблицы
     */
    int getTableSize();

    /**
     * опрос количества элементов в таблице
     */
    int getEntryAmount();

    /**
     * коэффициент α  - коэффициент заполнения таблицы (фактор заполнения таблицы)
     * α = n/m, где n – число занесенных в таблицу элементов, m – размер хеш-таблицы
     */
    double getLoadFactor();

    /**
     * запрос числа проб последней операции
     */
    int getProbeCount();

    /**
     * опрос пустоты таблицы
     */
    bool isEmpty();

    /**
     * очистка таблицы
     */
    void clear();

    /**
     * вставка элемента по ключу
     */
    bool insert(K key, T data);

    /**
     * поиск элемента по ключу
     */
    T find(K key);

    /**
     *  удаление элемента по ключу
     */
    bool remove(K key);

    /**
     * вывод структуры таблицы на печать
     */
    void print();


    /**
     * конструктор.
     * @param keysAmount -ожидаемое количество ключей
     */
    explicit HashTable(int keysAmount);

    /**
     * конструктор копирования
     */
    HashTable(const HashTable& other);

    ~HashTable();

    /**
     * Класс Итератор
     */
    class Iterator {
    private:
        HashTable<K, T> *collection;
        int index;
        int size;
    public:
        explicit Iterator(HashTable<K, T> *collection) : collection(collection) {
            this->size = collection->tableSize;
            this->index = 0;
        }

        void begin();
        void end();
        void next();
        bool hasNext();
        T* operator *(); //доступ к данным текущего элемента

    };
    friend class Iterator;

protected:
    int keysAmount; //ожидаемое количество ключей (n)
    int tableSize; //размер хэш таблицы (m) для заданного количества ключей
    int entryAmount; //текущее количество элементов

    int probeCount; //подсчет количества проб при зондировании
    int maxProbeAmount; //максимальное количество проб при зондировании, зависит от размера таблицы

    K *keyTable;
    T *dataTable;
    State *stateTable;

    static constexpr double factorC1 = 0.9d;
    static constexpr double factorC2 = 0.1d;

    const static int mersennePrimesAmount = 25;
    const int mersennePrimes[mersennePrimesAmount] = {1, 3, 7, 15, 31, 63, 127, 251, 509, 1021,
                                                      2039, 4093, 8191, 16381, 32749, 65521, 131071, 262139, 524287,
                                                      1048573,
                                                      2097143, 4194301, 8388593, 16777213, 33554393};

    //установить все элементы в свободное сосояние
    void setStatesToFree();

    bool isEqual(K keyOne, K keyTwo);


};



template<class K, class T>
bool HashTable<K, T>::isEqual(K keyOne, K keyTwo) {
    return std::fabs(keyOne - keyTwo) < std::numeric_limits<float>::epsilon();
}

template<class K, class T>
int HashTable<K, T>::hash(K key) {
    int result = (int) (key * 100) % tableSize;

    return result;
}

template<class K, class T>
HashTable<K, T>::HashTable(int keysAmount):keysAmount(keysAmount) {
    //Хеш-таблица с открытой адресацией, размер таблицы в два раза больше количества ключей

    int minTableSize = keysAmount * 2;
    int i = 0;
    while (mersennePrimes[i] < minTableSize && i < mersennePrimesAmount) {
        i++;
    }
    int mersennePrimeCurrent = (i < mersennePrimesAmount) ? mersennePrimes[i] : mersennePrimes[mersennePrimesAmount -
                                                                                               1];
    tableSize = mersennePrimeCurrent;
    probeCount = 0;
    maxProbeAmount = 10 * tableSize;
    entryAmount = 0;

    keyTable = new K[tableSize];
    dataTable = new T[tableSize];
    stateTable = new State[tableSize];

    setStatesToFree();

}

template<class K, class T>
HashTable<K, T>::HashTable(const HashTable& other) {
    tableSize = other.tableSize;
    probeCount = other.probeCount;
    maxProbeAmount = other.maxProbeAmount;
    entryAmount = other.entryAmount;

    keyTable = new K[tableSize];
    dataTable = new T[tableSize];
    stateTable = new State[tableSize];
    for (int i = 0; i < tableSize; i++) {
        keyTable[i] = other.keyTable[i];
        dataTable[i] = other.dataTable[i];
        stateTable[i] = other.stateTable[i];
    }

}

template<class K, class T>
HashTable<K, T>::~HashTable() {
    delete []keyTable;
    delete []stateTable;
    delete []dataTable;
}

template<class K, class T>
int HashTable<K, T>::getTableSize() {
    return this->tableSize;
}

template<class K, class T>
int HashTable<K, T>::getEntryAmount() {
    return this->entryAmount;
}

template<class K, class T>
double HashTable<K, T>::getLoadFactor() {
    return static_cast<double>(entryAmount) / tableSize;
}

template<class K, class T>
bool HashTable<K, T>::isEmpty() {
    return entryAmount == 0;
}

template<class K, class T>
void HashTable<K, T>::clear() {
    this->entryAmount = 0;
    setStatesToFree();
}

template<class K, class T>
void HashTable<K, T>::setStatesToFree() {
    for (int i = 0; i < tableSize; ++i) {
        stateTable[i] = State::free;
    }
}

template<class K, class T>
bool HashTable<K, T>::insert(K key, T data) {
    //h (k, i) = (h (k) + c1   i + c2   i2) mod m, где c1≠ 0  и c2 ≠ 0
    probeCount = 0; //первое зондирование
    int firstHashIndex = hash(key);
    int index = firstHashIndex;
    const int nullIndex = -1;
    int deletedIndex = nullIndex;
    //quadraticProbing
    while (State::free != stateTable[index] && probeCount < maxProbeAmount) {
        if (State::busy == stateTable[index] && isEqual(keyTable[index], key)) {
            //элемент с таким ключом уже присутствует в таблице
            return false;
        }
        if (State::deleted == stateTable[index] && nullIndex == deletedIndex) {
           deletedIndex = index;
        }
        probeCount++;
        index = (firstHashIndex + (int) (factorC1 * probeCount + factorC2 * probeCount * probeCount)) % tableSize;
    }

    if(probeCount == maxProbeAmount && nullIndex == deletedIndex){
        //достигли максимального количества проб, не встретив ячейки в состоянии удалено
        //все просмотренные ячейки были заняты и с другими ключами
        //не нашли место для вставки
        return false;
    }

    if(nullIndex != deletedIndex){
        //нашли ячейку в состоянии удалено, поместим новое значение в нее
        index = deletedIndex;
    }
        dataTable[index] = data;
        keyTable[index] = key;
        stateTable[index] = State::busy;
        entryAmount++;
        return true;
}

template<class K, class T>
bool HashTable<K, T>::remove(K key) {
    probeCount = 0; //первое зондирование
    int firstHashIndex = hash(key);
    int index = firstHashIndex;

    while (State::free != stateTable[index] && probeCount < maxProbeAmount) {
        if (State::busy == stateTable[index] && isEqual(keyTable[index], key)) {
            //элемент найден, удаляем
            stateTable[index] = State::deleted;
            dataTable[index] = 0;
            entryAmount--;
            return true;
        }
        probeCount++;
        index = (firstHashIndex + (int) (factorC1 * probeCount + factorC2 * probeCount * probeCount)) % tableSize;
    }
    return false;
}

template<class K, class T>
T HashTable<K, T>::find(K key) {
    probeCount = 0; //первое зондирование
    int firstHashIndex = hash(key);
    int index = firstHashIndex;

    while (State::free != stateTable[index] && probeCount < maxProbeAmount) {
        if (State::busy == stateTable[index] && isEqual(keyTable[index], key)) {
            //элемент найден
            return dataTable[index];
        }
        probeCount++;
        index = (firstHashIndex + (int) (factorC1 * probeCount + factorC2 * probeCount * probeCount)) % tableSize;
    }
    throw noSuchKeyException;

}

template<class K, class T>
void HashTable<K, T>::print() {
    if (isEmpty()) {
        std::cout << "hash table is empty" << std::endl;
        return;
    }
    for (int i = 0; i < tableSize; i++) {
        std::string entry = "";
        if (State::busy == stateTable[i]) {
            std::cout << "[" << i << "]" << "\t" << stateTable[i] << " " << std::to_string(keyTable[i]) << " " << std::to_string(dataTable[i]) << std::endl;
        } else {
            std::cout << "[" << i << "]" << "\t" << stateTable[i] << std::endl;
        }
    }

}

template<class K, class T>
int HashTable<K, T>::getProbeCount() {
    return probeCount + 1;
}

template<class K, class T>
void HashTable<K, T>::Iterator::begin() {
    if(collection->isEmpty()){
        throw noSuchElementException;
    }
    this->index = 0;
    while (collection->stateTable[index] != State::busy && hasNext() ) {
        index++;
    }
}

template<class K, class T>
void HashTable<K, T>::Iterator::next() {
    while (hasNext()) {
        index++;
        if(collection->stateTable[index] == State::busy){
            return;
        }
    }
    throw noSuchElementException;
}

template<class K, class T>
bool HashTable<K, T>::Iterator::hasNext() {
    return this->index + 1  < this->size;
}

template<class K, class T>
void HashTable<K, T>::Iterator::end() {
    if(collection->isEmpty()){
        throw noSuchElementException;
    }
    this->index = this->size; //устанавливаем на последний элемент
    while (collection->stateTable[index] != State::busy && index > 0 ) {
        --index;
    }
}

template<class K, class T>
T* HashTable<K, T>::Iterator::operator*() {
    return &collection->dataTable[index];
}

#endif //BST_AND_GRAPH_HASHTABLE_H
