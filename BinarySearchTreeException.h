#ifndef BST_AND_GRAPH_BINARYSEARCHTREEEXCEPTION_H
#define BST_AND_GRAPH_BINARYSEARCHTREEEXCEPTION_H

#include <exception>

class EmptyTreeException : public std::exception {
    virtual const char *what() const throw() {
        return "Error: Tree is empty";
    }

} emptyTreeException;

class NoSuchKeyException : public std::exception {
    virtual const char *what() const throw() {
        return "Error: The key is not found";
    }

} noSuchKeyException;

#endif //BST_AND_GRAPH_BINARYSEARCHTREEEXCEPTION_H
