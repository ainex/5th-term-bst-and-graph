#include <iostream>
#include <map>
#include <random>
#include "HashTable.h"
#include "HashTableTest.h"


const std::string S_CREATE = "create";
const std::string S_TABLE_SIZE = "tsize";
const std::string S_ENTRY_SIZE = "esize";
const std::string S_LOAD_FACTOR = "load";
const std::string S_IS_EMPTY = "empty";
const std::string S_CLEAR = "clear";
const std::string S_FIND = "find";
const std::string S_INSERT = "insert";
const std::string S_DELETE = "delete";
const std::string S_PRINT = "print";
const std::string S_PROBING = "probe";
const std::string S_ITERATOR = "iter";
const std::string S_ITERATOR_BEGIN = "iter_b";
const std::string S_ITERATOR_END = "iter_e";
const std::string S_ITERATOR_NEXT = "iter_n";
const std::string S_ITERATOR_UPDATE = "iter_up";
const std::string S_TEST_HASH = "htest";
const std::string S_TEST_OPERATION = "optest";

const std::string S_EXIT = "exit";
const std::string S_HELP = "help";

static constexpr double rangeStart = 100000.00;
static constexpr double rangeEnd = 150000.00;
enum Actions {
    CREATE,
    TABLE_SIZE,
    ENTRY_SIZE,
    LOAD_FACTOR,
    IS_EMPTY,
    CLEAR,
    FIND,
    INSERT,
    DELETE,
    PRINT,
    PROBING,
    ITERATOR,
    ITERATOR_BEGIN,
    ITERATOR_END,
    ITERATOR_NEXT,
    ITERATOR_UPDATE,
    TEST_HASH,
    TEST_OPERATION,
    EXIT,
    HELP
};

std::map<std::string, Actions> mapping;
std::random_device rd;

void initCommands();

void help();

void testHashFunction();

void testOperations();


int main() {
    initCommands();

    std::cout << "Hash Table Demo " << std::endl;
    std::cout << "Modular hash function (Mersenne Primes), Open addressing with Quadratic probing sequence"
              << std::endl;
    std::cout << "key = [100 000.00, +150 000.00]" << std::endl;

    help();

    HashTable<double, int> *hashTable = new HashTable<double, int>(3);
    hashTable->insert(100000.001, 1);
    hashTable->insert(100000.002, 2);
    hashTable->insert(100000.003, 3);

    HashTable<double, int>::Iterator *it = new HashTable<double, int>::Iterator(hashTable);
    std::cout << "a demo hash table created, keys amount (n) = 3, size (m) = " << hashTable->getTableSize()
              << std::endl;

    std::string command = "";

    while (true) {

        std::cout << ">> " << std::endl;
        std::cin >> command;
        std::map<std::string, Actions>::iterator commandIterator = mapping.find(command);
        Actions action = Actions::HELP;
        if (commandIterator != mapping.end()) {
            action = commandIterator->second;
        }

        try {
            switch (action) {
                case Actions::CREATE: {
                    delete hashTable;
                    std::cout << "insert keys amount (n)" << std::endl;
                    int keysNumber;
                    std::cin >> keysNumber;
                    hashTable = new HashTable<double, int>(keysNumber);
                    std::cout << "a new hash table created, keys amount (n) = " << keysNumber << std::endl;
                    break;
                }
                case Actions::TABLE_SIZE: {
                    std::cout << "hash table size (m) = " << hashTable->getTableSize() << std::endl;
                    break;
                }
                case Actions::ENTRY_SIZE: {
                    std::cout << "entries amount  = " << hashTable->getEntryAmount() << std::endl;
                    break;
                }
                case Actions::LOAD_FACTOR: {
                    std::cout << "load factor (entries/m)  = " << hashTable->getLoadFactor() << std::endl;
                    break;
                }
                case Actions::IS_EMPTY: {
                    std::cout << "is empty:  " << hashTable->isEmpty() << std::endl;
                    break;
                }
                case Actions::CLEAR: {
                    hashTable->clear();
                    hashTable->print();
                    break;
                }
                case Actions::INSERT: {
                    std::cout << "insert key and data (k, v)" << std::endl;
                    int data;
                    double key;
                    std::cin >> key >> data;
                    bool result = hashTable->insert(key, data);
                    std::cout << "insert result: " << result << std::endl;
                    break;
                }
                case Actions::FIND: {
                    std::cout << "enter a key to find (k)" << std::endl;

                    double key;
                    std::cin >> key;
                    int data = hashTable->find(key);
                    std::cout << "found by key: " << data << std::endl;
                    break;
                }
                case Actions::DELETE: {
                    std::cout << "insert key to delete (k)" << std::endl;

                    double key;
                    std::cin >> key;
                    bool result = hashTable->remove(key);
                    std::cout << "delete result: " << result << std::endl;
                    break;
                }

                case Actions::PRINT: {
                    hashTable->print();
                    break;
                }
                case Actions::PROBING: {
                    std::cout << hashTable->getProbeCount() << std::endl;
                    break;
                }
                case Actions::ITERATOR: {
                    it = new HashTable<double, int>::Iterator(hashTable);
                    it->begin();
                    std::cout << "new iterator" << std::endl;
                    break;
                }
                case Actions::ITERATOR_BEGIN: {
                    it->begin();
                    std::cout << "iter begin entry: " << ***it << std::endl;
                    break;
                }
                case Actions::ITERATOR_END: {
                    it->end();
                    //int *data = **it;
                    std::cout << "iter end entry: " << ***it << std::endl;
                    break;
                }
                case Actions::ITERATOR_NEXT: {
                    it->next();
                    //int *data = **it;
                    std::cout << "iter next entry: " << ***it << std::endl;
                    break;
                }
                case Actions::ITERATOR_UPDATE: {
                    std::cout << "enter new data to update iter current entry: " << ***it << std::endl;

                    int newData;
                    std::cin >> newData;
                    int *data = **it;
                    *data = newData;
                    std::cout << "iter  entry: " << ***it << std::endl;
                    break;
                }
                case Actions::TEST_HASH: {
                    testHashFunction();
                    break;
                }
                case Actions::TEST_OPERATION: {
                    testOperations();
                    break;
                }
                case Actions::EXIT: {
                    delete hashTable;
                    exit(0);
                    break;
                }
                case Actions::HELP: {
                    help();
                    break;
                }
                default:
                    help();
                    break;
            }
        }
        catch (const std::exception &e) {
            std::cerr << e.what() << std::endl;
        }
    }

}

void initCommands() {
    mapping[S_CREATE] = Actions::CREATE;
    mapping[S_TABLE_SIZE] = Actions::TABLE_SIZE;
    mapping[S_ENTRY_SIZE] = Actions::ENTRY_SIZE;
    mapping[S_LOAD_FACTOR] = Actions::LOAD_FACTOR;
    mapping[S_IS_EMPTY] = Actions::IS_EMPTY;
    mapping[S_CLEAR] = Actions::CLEAR;
    mapping[S_FIND] = Actions::FIND;
    mapping[S_INSERT] = Actions::INSERT;
    mapping[S_DELETE] = Actions::DELETE;
    mapping[S_PRINT] = Actions::PRINT;
    mapping[S_PROBING] = Actions::PROBING;
    mapping[S_ITERATOR] = Actions::ITERATOR;
    mapping[S_ITERATOR_BEGIN] = Actions::ITERATOR_BEGIN;
    mapping[S_ITERATOR_END] = Actions::ITERATOR_END;
    mapping[S_ITERATOR_NEXT] = Actions::ITERATOR_NEXT;
    mapping[S_ITERATOR_UPDATE] = Actions::ITERATOR_UPDATE;
    mapping[S_TEST_HASH] = Actions::TEST_HASH;
    mapping[S_TEST_OPERATION] = Actions::TEST_OPERATION;
    mapping[S_EXIT] = Actions::EXIT;
    mapping[S_HELP] = Actions::HELP;
}


void help() {
    std::cout << "Commands' List: " << std::endl;
    std::cout << S_CREATE << "\t  - create a new hash table" << std::endl;
    std::cout << S_TABLE_SIZE << "\t  - get hash table size" << std::endl;
    std::cout << S_ENTRY_SIZE << "\t  - get entries amount" << std::endl;
    std::cout << S_LOAD_FACTOR << "\t  - get load factor" << std::endl;
    std::cout << S_IS_EMPTY << "\t  - check if table is empty" << std::endl;
    std::cout << S_CLEAR << "\t  - clear the hash table" << std::endl;
    std::cout << S_FIND << "\t  - find value by key" << std::endl;
    std::cout << S_INSERT << "\t  - insert value by key (k, v)" << std::endl;
    std::cout << S_DELETE << "\t  - delete value by key" << std::endl;

    std::cout << S_PRINT << "\t  - print table" << std::endl;
    std::cout << S_PROBING << "\t  - get last operation probing amount" << std::endl;
    std::cout << S_ITERATOR << "\t  - get iterator; " << S_ITERATOR_BEGIN << " - begin; " << S_ITERATOR_END
              << " - end; " << S_ITERATOR_NEXT << " - next; " << S_ITERATOR_UPDATE << " - update" << std::endl;
    std::cout << S_TEST_HASH << "\t  - test hash function" << std::endl;
    std::cout << S_TEST_OPERATION << "\t  - test operation" << std::endl;

    std::cout << S_EXIT << "\t  - exit" << std::endl;
    std::cout << S_HELP << "\t  - repeat this message" << std::endl;

}

void testHashFunction() {
    std::cout << "insert keys amount (n)" << std::endl;
    int keysNumber;
    std::cin >> keysNumber;
    HashTable<double, int> *hashTable = new HashTable<double, int>(keysNumber);
    std::cout << "table size: " << hashTable->getTableSize() << std::endl;
    int m = hashTable->getTableSize();
    int keysMultiply = 20;
    int p = keysMultiply * m;

    auto *hashes = new int[m];
    for (int n = 0; n < m; ++n) {
        hashes[n] = 0;
    }


    // Engine

    std::mt19937 engine(rd());
    // Distributions
    std::uniform_real_distribution<> dist(rangeStart, rangeEnd);


    for (int n = 0; n < p; ++n) {
        double randomKey = dist(engine);
        int index = hashTable->hash(randomKey);
        hashes[index] = hashes[index] + 1;
    }


    double xi = 0.0;
    for (int n = 0; n < m; ++n) {
        xi = xi + (hashes[n] - keysMultiply) * (hashes[n] - keysMultiply);
    }
    xi = xi / keysMultiply;
    std::cout << "square xi: " << xi << std::endl;

    delete hashTable;


}

void testOperations() {
    HashTable<double, int> *hashTable = new HashTable<double, int>(1000);
    std::cout << "key amount n = 1000, m = " << hashTable->getTableSize() << std::endl;
    std::cout << "enter load factor a: " << std::endl;
    double loadFactor;
    std::cin >> loadFactor;

    auto *test = new HashTableTest<double, int>();
    test->testOperations(loadFactor, hashTable);

}

/* HashTable<double, int> hashTableToCopy(3);
    hashTableToCopy.insert(100002.001, 1);
    hashTableToCopy.insert(100002.002, 2);
    hashTableToCopy.insert(100002.003, 3);
    HashTable<double, int> hashTableCopy(hashTableToCopy);
   hashTableCopy.insert(100002.004, 4);
    hashTableCopy.print();
    hashTableToCopy.print();*/





